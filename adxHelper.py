from azure.kusto.data import KustoClient, KustoConnectionStringBuilder
import json
import getpass
import keyring
import logging

logging.basicConfig(level=logging.DEBUG, filename='adx.log')

try:
    configFile = open("config_adx.json")
    logging.info("Reading configuration from: " + configFile.name)

    configJson = json.load(configFile)
    AAD_TENANT_ID = configJson["AAD_TENANT_ID"]
    KUSTO_URI = configJson["KUSTO_URI"]
    KUSTO_INGEST_URI = configJson["KUSTO_INGEST_URI"]
    CLIENT_ID = configJson["CLIENT_ID"]
    CLIENT_SECRET = keyring.get_password("CLIENT_ID", CLIENT_ID)
    logging.info("Read Password for CLIENT_ID: " + CLIENT_ID + " for AAD_TENANT_ID: " + AAD_TENANT_ID)

except IOError:
    logging.info("No config present. Creating one.")

    configJson = {}
    AAD_TENANT_ID = input("AAD_TENANT_ID: ").strip()
    CLIENT_ID = input("CLIENT_ID: ").strip()
    CLIENT_SECRET = getpass.getpass(prompt="CLIENT_SECRET: ").strip()
    KUSTO_URI = input("KUSTO_URI: ")
    KUSTO_INGEST_URI = input("KUSTO_INGEST_URI: ")
    configJson["AAD_TENANT_ID"] = AAD_TENANT_ID
    configJson["KUSTO_URI"] = KUSTO_URI
    configJson["KUSTO_INGEST_URI"] = KUSTO_INGEST_URI
    configJson["CLIENT_ID"] = CLIENT_ID
    configFile = open("config_adx.json", "w")
    json.dump(configJson, configFile)
    configFile.close()
    keyring.set_password("CLIENT_ID", CLIENT_ID, CLIENT_SECRET)
    logging.info("Set Password for CLIENT_ID: " + CLIENT_ID + " for AAD_TENANT_ID: " + AAD_TENANT_ID + " in " + configFile.name)
    
finally:
    configFile.close()

def createDataClient():
    KCSB_DATA = KustoConnectionStringBuilder.with_aad_application_key_authentication(KUSTO_URI, CLIENT_ID, CLIENT_SECRET, AAD_TENANT_ID)
    KUSTO_DATA_CLIENT = KustoClient(KCSB_DATA)

    return KUSTO_DATA_CLIENT

def getBearerToken(KUSTO_CLIENT):
    BEARER_TOKEN = KUSTO_CLIENT._auth_provider.acquire_authorization_header().__str__()
    logging.debug(BEARER_TOKEN)

    return BEARER_TOKEN

def createIngestClient():
    KCSB_INGEST = KustoConnectionStringBuilder.with_aad_application_key_authentication(KUSTO_INGEST_URI, CLIENT_ID, CLIENT_SECRET, AAD_TENANT_ID)
    KUSTO_INGEST_CLIENT = KustoClient(KCSB_INGEST)

    return KUSTO_INGEST_CLIENT

def createTable(KUSTO_CLIENT, KUSTO_DATABASE, TABLE_NAME, COLUMN_DEFINITION):
    KCSB_DATA = KustoConnectionStringBuilder.with_aad_application_key_authentication(KUSTO_URI, CLIENT_ID, CLIENT_SECRET, AAD_TENANT_ID)

    KUSTO_CLIENT = KustoClient(KCSB_DATA)
    CREATE_TABLE_COMMAND = ".create table {} ({})".format(TABLE_NAME, COLUMN_DEFINITION)
    logging.debug(CREATE_TABLE_COMMAND)

    RESPONSE = KUSTO_CLIENT.execute_mgmt(KUSTO_DATABASE, CREATE_TABLE_COMMAND)
    logging.debug(RESPONSE.primary_results[0])

    return RESPONSE

def dropTable(KUSTO_CLIENT, KUSTO_DATABASE, TABLE_NAME):
    DROP_TABLE_COMMAND = ".drop table {}".format(TABLE_NAME)
    logging.debug(DROP_TABLE_COMMAND)

    RESPONSE = KUSTO_CLIENT.execute_mgmt(KUSTO_DATABASE, DROP_TABLE_COMMAND)
    logging.debug(RESPONSE.primary_results[0])

    return RESPONSE

def executeQuery(KUSTO_CLIENT, KUSTO_DATABASE, QUERY):
    logging.debug(QUERY)

    RESPONSE = KUSTO_CLIENT.execute(KUSTO_DATABASE, QUERY)
    logging.debug(RESPONSE.primary_results[0])

    return RESPONSE

def executeMgmtQuery(KUSTO_CLIENT, KUSTO_DATABASE, MGMT_QUERY):
    logging.debug(MGMT_QUERY)

    RESPONSE = KUSTO_CLIENT.execute_mgmt(KUSTO_DATABASE, MGMT_QUERY)
    logging.debug(RESPONSE.primary_results[0])

    return RESPONSE

#KUSTO_DATA_CLIENT = createDataClient()
#KUSTO_INGEST_CLIENT = createIngestClient()
#createTable(KUSTO_CLIENT, "kn", "testTable", "timestamp: datetime, reason: string, event_id: long")
#dropTable(KUSTO_CLIENT, "kn", "testTable")